module Disklord
  module Models
    class VolumeSet < Sequel::Model(:volume_sets)
      module Status
        # All EBS volumes are detached from any EC2 instances.
        DETACHED = :detached

        # All EBS volumes are attached to the same EC2 instance and
        # they are attached to the correct mount points.
        ATTACHED = :attached

        # At least one volume is attached to an EC2 instance and is
        # attached to the correct mount point, while all other volumes
        # are detached.
        PARTIALLY_ATTACHED = :partially_attached

        # Volumes are attached to multiple EC2 instances _or_ volumes
        # are attached to the wrong mount points on the right EC2 instance.
        # Some volumes may be unattached). A forcible detach operation
        # will detach all volumes from all instances, but this
        # probably should be done by a human instead.
        CONFUSED = :confused

        # One or more EBS volumes have been deleted.
        # This is non-recoverable without human interaction.
        DAMAGED = :damaged
      end

      many_to_one :pool,    class: 'Disklord::Models::VolumeSetPool',
                            key: :pool_id

      one_to_many :volumes, class: 'Disklord::Models::VolumeSetVolume',
                            key: :volume_set_id

      eager :pool
      eager :volumes

      nested_attributes :volumes

      def state
        state_from_volume_states(volume_states)
      end

      def to_response_hash
        working = to_hash.dup.merge(
          volumes: volumes.map { |vs| [ vs.name, vs.to_response_hash.except!(:name) ] }.to_h
        ).except!(:id, :pool_id)

        working[:state] = state_from_volume_states(working[:volumes].values.map {|v| v[:state]} )

        working
      end

      private
      def volume_states
        # TODO: explore the Parallel gem (to multiplex the AWS calls); has threading problems w/sqlite.
        volumes.map(&:to_response_hash)
        # Parallel.map(volumes, &:to_response_hash)
      end

      def state_from_volume_states(volume_states)
        ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

        relevant_instances = volume_states.map { |vs| vs[:instances] }.flatten.uniq.reject(&:nil?)

        status =
          # any missing volumes means we're damaged.
          if volume_states.any? { |vs| vs[:status] == :deleted }
            :damaged
          # if we're not damaged but not attached to anything, we are detached.
          elsif relevant_instances.empty?
            :detached
          # if any volumes are confused (mounted to the wrong mount points or
          # are on different instances), we are confused.
          elsif relevant_instances.size > 1 ||
                volume_states.any? { |vs| vs[:status] == :confused }
            :confused
          # if some of us are attached to an instance but not all, we are
          # partially attached.
          elsif relevant_instances == 1 &&
                volume_states.any? { |vs| !vs[:instances].include?(relevant_instances.first) }
            :partially_attached
          # if all of us are attached to the same instance, we are attached.
          elsif relevant_instances == 1 &&
                volume_states.all? { |vs| vs[:instances].include?(relevant_instances.first) }
            :attached
          end

        { status: status, instances: relevant_instances }
      end
    end
  end
end
