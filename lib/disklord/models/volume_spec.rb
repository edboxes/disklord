module Disklord
  module Models
    class VolumeSpec < Sequel::Model(:volume_specs)
      many_to_one :pool,    class: 'Disklord::Models::VolumeSetPool',
                            key: :pool_id

      one_to_many :volumes, class: 'Disklord::Models::VolumeSetVolume',
                            key: :volume_set_id

      def validate
        super

        validates_presence [:name, :description, :mount_point, :gigabytes, :ebs_type]

        validates_unique([:pool_id, :name], [:pool_id, :mount_point])
        validates_integer :gigabytes
        validates_operator(:>, 0, :gigabytes)
        validates_includes(['gp2', 'st1', 'sc1', 'standard'], :ebs_type)

        errors.add(:piops, "not yet supported, should be nil") unless piops.nil?
      end
    end
  end
end
