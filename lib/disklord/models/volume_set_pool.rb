module Disklord
  module Models
    class VolumeSetPool < Sequel::Model(:volume_set_pools)
      include Disklord::Logging::Mixin

      one_to_many :sets,  class: 'Disklord::Models::VolumeSet',
                          key: :pool_id
      one_to_many :specs, class: 'Disklord::Models::VolumeSpec',
                          key: :pool_id

      one_to_many :availability_zones,  class: 'Disklord::Models::VolumeSetPoolAZ',
                                        key: :pool_id

      nested_attributes :specs
      nested_attributes :availability_zones

      def validate
        super

        validates_presence [:name, :description]

        validates_unique(:name)
        errors.add(:specs, "must have at least one spec") if specs.empty?

        spec_names = specs.map(&:name)
        spec_mount_points = specs.map(&:mount_point)
        errors.add(:specs, "must all have unique names") \
          if spec_names.length != spec_names.uniq.length
        errors.add(:specs, "must all have unique mount points") \
          if spec_mount_points.length != spec_mount_points.uniq.length

        errors.add(:availability_zones, "must have at least one entry") if availability_zones.empty?
      end

      def count
        sets.count
      end

      def expand!(new_count:)
        raise "new_count must be an Integer and larger than the current count (#{count})." \
          unless new_count.is_a?(Integer) && new_count > count

        info_log "pool '#{name}': expanding from #{count} to #{new_count}"

        ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

        next_ordinal = sets.empty? ? 0 : (sets.map(&:ordinal).max + 1)

        sets_to_add = new_count - count

        new_sets = []

        # beware the fencepost error
        (0...sets_to_add).each do |n|
          DB.transaction do
            ordinal = n + next_ordinal
            info_log "pool '#{name}': creating volume set #{ordinal}"

            vs = VolumeSet.new(pool: self, ordinal: ordinal, availability_zone: next_az_for_set)

            vs.save

            begin
              specs.each do |spec|
                info_log "pool '#{name}', set #{ordinal}: creating volume for spec '#{spec.name}'"
                begin
                  ebs_volume =
                    ec2_rsrc.create_volume(
                      availability_zone: vs.availability_zone,

                      size: spec.gigabytes,
                      volume_type: spec.ebs_type,
                      iops: spec.piops, # TODO: when piops are supported, this will be non-nil

                      encrypted: spec.encrypted,

                      snapshot_id: spec.snapshot_id,
                      kms_key_id: spec.kms_key_id
                    )

                  vsv = VolumeSetVolume.new
                  vsv.set = vs
                  vsv.spec = spec
                  vsv.ebs_id = ebs_volume.id

                  vsv.save

                  vsv.ensure_tags
                rescue StandardError => err
                  error_log "pool '#{name}', set #{ordinal}: error creating " \
                            "volume '#{spec.name}': #{err}"

                  begin
                    error_log "pool '#{name}', set #{ordinal}, spec '#{spec.name}': trying to " \
                              "clean up orphaned volume #{ebs_volume.id}"
                    ebs_volume.delete
                  rescue Aws::EC2::Errors::InvalidVolumeNotFound; end

                  raise err
                end
              end

              vs.save

              new_sets << vs
              sets << vs
            rescue StandardError => err
              error_log "pool '#{name}', set #{ordinal}: error creating: #{err}"
              error_log "cleaning up new volumes"

              vs.volumes.each do |volume|
                begin
                  error_log "pool '#{name}', set #{ordinal}: deleting volume '#{volume.ebs_id}'"
                  ec2_rsrc.volume(volume.ebs_id).delete
                rescue Aws::EC2::Errors::InvalidVolumeNotFound; end
              end

              raise Sequel::Rollback
            end

            save
          end
        end

        new_sets
      end

      def to_response_hash
        to_hash.dup.merge!(
          set_count: sets.size,
          specs: specs.map { |s| [ s.name, s.to_hash.dup.except!(:name) ] }.to_h,
          availability_zones: availability_zones.map(&:availability_zone)
        ).except!(id)
      end

      private
      # The idea here is to find the next place to place a volume set. Lowest count wins; tiebreaker
      # is the order of the availability zones when the VolumeSetPool was specified.
      def next_az_for_set
        azs = availability_zones.map(&:availability_zone)
        counts = azs.map { |az| [az, 0] }.to_h

        sets.each do |set|
          counts[set.availability_zone] += 1
        end

        min_count = counts.values.min
        candidates = counts.select { |k, v| v == min_count }

        candidates.keys.sort_by { |az| azs.find_index(az) }.first
      end
    end
  end
end
