module Disklord
  module Models
    class VolumeSetVolume < Sequel::Model(:volume_set_volumes)
      many_to_one :set, class: 'Disklord::Models::VolumeSet',
                        key: :volume_set_id

      many_to_one :spec,  class: 'Disklord::Models::VolumeSpec',
                          key: :volume_spec_id

      eager :set
      eager :spec

      def name
        spec.name
      end

      def mount_point
        spec.mount_point
      end

      def ensure_tags
        ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

        ec2_rsrc.volume(ebs_id).create_tags(
          tags: {
            'disklord:pool' => set.pool.name,
            'disklord:set' => set.ordinal,
            'disklord:volume_name' => spec.name,
            'disklord:volume_mount_point' => spec.mount_point
          }.map { |k, v| { key: k, value: v.to_s} }
        )
      end

      def state
        ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

        begin
          ebs_volume = ec2_rsrc.volume(ebs_id)
          ebs_attachments = ebs_volume.attachments

          if ebs_attachments.empty? || ebs_attachments.all? { |att| att.state == 'detached' }
            { status: :detached }
          else
            active_attachments =
              ebs_attachments.select do |att|
                %w(attached attaching detaching).include?(att.state)
              end

            status =
              case active_attachments.all? { |att| att.device == spec.mount_point }
              when true
                :attached
              when false
                :confused
              end

            {
              status: status,
              instances:
                ebs_attachments.select do |att|
                  %w(attached attaching detaching).include?(att.state)
                end.map(&:instance_id)
            }
          end

        rescue Aws::EC2::Errors::InvalidVolumeNotFound
          { status: :deleted }
        end
      end

      def to_response_hash
        to_hash.dup.merge(state: state, name: spec.name).except!(:id, :volume_set_id, :volume_spec_id)
      end
    end
  end
end
