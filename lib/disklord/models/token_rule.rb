module Disklord
  module Models
    class TokenRule < Sequel::Model(:token_rules)
      many_to_one :token, class: 'Disklord::Models::Token',
                          key: :token_id

      # TODO: these helper methods are totally inefficient; they should
      #       be replaced with better ones that cache the result and
      #       have a dirty bit for when `rule` is updated.
      def type
        JSON.parse(rule)['type']
      end

      def actions
        JSON.parse(rule)['actions']
      end

      def names
        JSON.parse(rule)['names'].map { |i| Regexp.new(i) }
      end
    end
  end
end
