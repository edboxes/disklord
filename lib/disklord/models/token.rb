require 'disklord/models/token_rule'

require 'uuidtools'

module Disklord
  module Models
    class Token < Sequel::Model(:tokens)

      one_to_many :rules, class: 'Disklord::Models::TokenRule',
                          key: :token_id


      def authorized?(type:, name:, action:)
        raise "type must be a String" unless type.is_a?(String)
        raise "name must be a String" unless name.is_a?(String)
        raise "action must be a String" unless action.is_a?(String)

        rules.map { |r| JSON.parse(r.rule) } \
              .any? do |jr|
                jr['type'] == type &&
                  jr['names'].any? { |n| Regexp.new(n) =~ name } &&
                  !([ 'all', action ] & jr['actions']).empty?
              end
      end

      def self.generate(content)
        token = Token.new(value: UUIDTools::UUID.timestamp_create,
                          issued_on: DateTime.now).save

        content.each do |rule|
          tr = TokenRule.new

          tr.token = token
          tr.rule = rule.to_json

          tr.save
        end

        token
      end
    end
  end
end
