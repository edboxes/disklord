module Disklord
  module Models
    class VolumeSetPoolAZ < Sequel::Model(:volume_set_pool_azs)
      many_to_one :pool,    class: 'Disklord::Models::VolumeSetPool',
                            key: :pool_id
    end
  end
end
