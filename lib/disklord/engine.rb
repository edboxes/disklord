require 'sequel'

module Disklord
  class Engine
    include Disklord::Logging::Mixin
    extend Disklord::Logging::Mixin

    attr_reader :policies

    def initialize
      policy_dir = Disklord::Settings.security.policy_directory

      @policies =
        Dir["#{policy_dir}/*.json"].map do |f|
          Wonk::Policy.from_hash(f, JSON.parse(IO.read(f)))
        end.freeze
    end

    def authenticate_from_submission(submission)
      submission = IceNine.deep_freeze(submission.deep_dup.deep_symbolize_keys)

      policy_results =
        policies.map do |p|
          [ p, p.authenticate_from_submission(submission) ]
        end.select { |pt| pt[1].success? }

      if !policy_results.empty?
        rules = policy_results.map { |pt| pt[1].concretized_content }.flatten

        token = Disklord::Models::Token.generate(rules)
        token
      else
        nil
      end
    end
  end
end
