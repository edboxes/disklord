module Disklord
  class Backend
    def read(key)
      raise "#{self.class.name}#read(key) must be implemented."
    end

    def write(key, hash)
      raise "#{self.class.name}#write(key, hash) must be implemented."
    end

    def self.from_settings
      backend_class = Disklord::Settings.backend.class_name.constantize
      parameters = Disklord::Settings.backend.parameters

      backend_class.new(parameters)
    end
  end
end
