# API Documentation #

Disklord uses a pretty straightforward JSON API. (TODO: refactor into a Swagger API?) There are three classes of API calls: health check, authentication, and general activity.

## Health Check ##
`GET /v1/health-check` will return a blurb of non-sensitive information that indicates that the server is up and functional. (TODO: Potentially check for AWS API access?)

#### Return Values ####
- 200: OK
- anything else: problem


## Authentication ##
Authentication is `POST /v1/authenticate` with a JSON body that varies based on the type of authentication being used by the principal in your policy.

Username/password auth:

```
{
    "authentication_type": "username-password",
    "username": "admin",
    "password": "password"
}
```

AWS EC2 auth:

```
{
    "authentication_type": "aws-ec2",
    "document": "contents of http://169.254.169.254/latest/dynamic/instance-identity/document",
    "signature": "contents of http://169.254.169.254/latest/dynamic/instance-identity/pkcs7"
}
```

#### Return Values ####
- 200: will return a JSON object; the `token` field is your access token.
- 401: authentication failure


## General ##
When using any general API, pass the token from the authentication step as the header `X-Disklord-Token`. Any method may return 401 if the token is omitted, invalid, expired, or revoked; most methods will return 403 if the token's principal does not have access

### List EBS Volume Pools ###
`GET /v1/pools` enumerates every volume pool for which the authenticated principal has the `info` action.

#### Return Values ###
- 200: a JSON array of volume pool names

### Create EBS Volume Pool ###
`POST /v1/pools` allows the creation of a new pool. (This could have been a `PUT /v1/pools/POOLNAME`, but that conflates with updates and updates are potentially very destructive.) The request should look like the following:

```
{
    "name": "volume-pool-00",
    "description": "pool for system #00",
    "specs": [
        {
            "name": "data1",
            "description": "data storage for the gizmo",
            "encrypted": false,
            "mount_point": "/dev/xvdg",
            "gigabytes": 40,
            "ebs_type": gp2
        }
    ],
    "initial_count": 3
}
```

NOTE: This actually operates in two steps: first, it builds the volume pool, then
expands it to `initial_count`.
TODO: support `io1` and PIOPS.