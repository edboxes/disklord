require 'sequel'

Sequel::Model.plugin :json_serializer
Sequel::Model.plugin :nested_attributes
Sequel::Model.plugin :validation_helpers
Sequel::Model.plugin :dirty

Dir["#{__dir__}/models/**/*.rb"] \
  .each { |f| require_relative f }
