require 'disklord/api/errors'

module Disklord
  module API
    class V1 < Grape::API
      TOKEN_HEADER_NAME = "X-Disklord-Token"

      logger Disklord::Logging.logger

      format :json
      version :v1, type: :path

      rescue_from BadRequestError do |e|
        error!(e.message, 400)
      end

      rescue_from UnauthorizedError do |e|
        error!(e.message, 401, 'WWW-Authenticate' => '/v1/authenticate')
      end

      rescue_from ForbiddenError do |e|
        error!(e.message, 403)
      end

      rescue_from NotFoundError do |e|
        error!(e.message, 404)
      end

      rescue_from InternalServerError do |e|
        error!(e.message, 500)
      end

      helpers do
        def logger
          Disklord::Logging.logger
        end

        def engine
          @engine ||= Disklord::Engine.new
        end

        def token_expiry_duration
          @token_expiry_duration ||=
            ChronicDuration.parse(Disklord::Settings.security.token_duration, keep_zero: true)
        end

        def authenticate!(&block)
          token = Models::Token.first(value: request.headers[TOKEN_HEADER_NAME])

          raise UnauthorizedError, "#{TOKEN_HEADER_NAME} not passed" if token.nil?

          if token.revoked || (token.issued_on + token_expiry_duration.seconds) < DateTime.now
            revoke_token(token)
            raise UnauthorizedError, "token is invalid"
          end

          env['disklord.token'] = token

          if block.nil?
            token
          else
            block.call(token)
          end
        end

        def revoke_token(token)
          raise "'token' must be a Disklord::Models::Token." \
            unless token.is_a?(Models::Token)

          token.revoked = true
          token.save
          token
        end

        def request_body_as_json
          begin
            JSON.parse(request.body.read).deep_symbolize_keys
          rescue
            raise BadRequestError, "error parsing request body - was it valid JSON?"
          end
        end

        def authorize!(type:, name:, action:, &block)
          authenticate! do |token|
            if token.authorized?(type: type, name: name, action: action.to_s)
              if block.nil?
                nil
              else
                yield
              end
            else
              raise ForbiddenError, "can't invoke '#{action}' on resource '#{type}' named '#{name}'."
            end
          end
        end
      end

      desc 'Performs a health check on the service and returns 200 with service info.'
      get 'health-check' do
        {
          version: Disklord::VERSION,
          engine: {
            validator_count: Wonk::Policy::VALIDATORS.count,
            policy_count: engine.policies.count
          }
        }
      end

      desc 'Authenticates with multiple principal backends to get an access token with permissions.'
      post 'authenticate' do
        submission = request_body_as_json
        token = engine.authenticate_from_submission(submission)

        if token.nil?
          raise UnauthorizedError, "invalid authentication"
        else
          { token: token.value }
        end
      end

      namespace :pools do
        desc 'Lists all pools by name'
        get do
          authenticate! do |token|
            list_id_regexps =
              token.rules.reject do |r|
                (%w(list all) & r.actions).empty?
              end.map { |r| r.names }.flatten

            begin
              Models::VolumeSetPool.where(list_id_regexps.map { |r| [:name, r ] }).to_a
            rescue Sequel::InvalidOperation # sequel doesn't support regexp matches under sqlite, so fall back
              Models::VolumeSetPool.all.select do |vsp|
                list_id_regexps.any? { |r| r =~ vsp.name }
              end
            end.map(&:name)
          end
        end

        desc 'Creates a new pool'
        post do
          resource_type = 'ebs-volume-pool'

          body = request_body_as_json
          resource_name = body[:name]
          raise BadRequestError, "'name' is required." if resource_name.nil?

          authorize!(type: resource_type, name: resource_name, action: :create) do
            raise BadRequestError, "#{resource_type} with name '#{resource_name}' already exists." \
              unless Models::VolumeSetPool.first(name: resource_name).nil?

            initial_count = body[:initial_count] || 0
            body.delete(:initial_count)

            body[:specs_attributes] = body[:specs] # cleaning up for nested attributes
            body.delete(:specs)
            body[:availability_zones_attributes] = # ditto
              (body[:availability_zones] || []).map { |az| { "availability_zone" => az } }
            body.delete(:availability_zones)

            logger.info "Creating new #{resource_type} with name '#{resource_name}', initial count #{initial_count}."

            vsp = Models::VolumeSetPool.new(body)
            vsp.save

            logger.info "#{vsp.name} successfully created."

            if initial_count > 0
              logger.info "resizing new pool #{vsp.name} to #{initial_count}."
              new_volume_sets = vsp.expand!(new_count: initial_count)
              logger.info "pool #{vsp.name}: #{new_volume_sets.size} created."
            end

            vsp.to_response_hash
          end
        end

        params do
          requires :pool_name, type: String, desc: 'VolumeSetPool name'
        end
        route_param :pool_name do
          desc 'Returns information about a VolumeSetPool.'
          get do
            resource_type = 'ebs-volume-pool'

            authorize!(type: resource_type, name: params['pool_name'], action: :info) do
              p = Models::VolumeSetPool.first(name: params['pool_name'])
              raise NotFoundError if p.nil?

              p.to_response_hash
            end
          end

          # TODO: hoist this stuff out of the API to make disklord usable as an API
          desc 'Updates a VolumeSetPool (generally, volume set count).'
          patch do
            resource_type = 'ebs-volume-pool'
            changes = request_body_as_json

            authorize!(type: resource_type, name: params['pool_name'], action: :alter) do
              p = Models::VolumeSetPool.first(name: params['pool_name'])
              raise NotFoundError if p.nil?

              models_to_save = []

              p.description = changes[:description] || p.description

              unless p.changed_columns.empty?
                logger.info "'#{p.initial_value(:name)}': updating meta"
                models_to_save << p
              end


              spec_changes = changes[:specs]
              unless spec_changes.empty?
                specs = p.specs.map { |s| [ s.name, s] }.to_h.stringify_keys

                spec_changes.each_pair do |spec_name, spec_change|
                  spec = specs[spec_name.to_s]

                  # TODO: PIOPS updates
                  # TODO: allow mount point changes, but only if all sets are detached
                  # TODO: do something smart if the user changes KMS id but not snapshot id
                  spec.gigabytes = spec_change[:gigabytes] || spec.gigabytes
                  spec.snapshot_id = spec_change[:snapshot_id] || spec.snapshot_id
                  spec.kms_key_id = spec_change[:kms_key_id] || spec.kms_key_id

                  unless spec.changed_columns.empty?
                    logger.info "'#{p.initial_value(:name)}': updating volume spec '#{spec.name}'"
                    models_to_save << spec
                  end
                end
              end

              unless models_to_save.empty?
                logger.info "'#{p.initial_value(:name)}': updates made; entering update transaction"

                ::DB.transaction do
                  models_to_save.each { |m| m.save }
                end
              end

              p.to_response_hash
            end
          end

          desc 'requests an attachment of a volume set to an instance.'
          post '/attach' do
            resource_type = 'ebs-volume-pool'
            body = request_body_as_json

            authorize!(type: resource_type, name: params['pool_name'], action: :attach) do
              p = Models::VolumeSetPool.first(name: params['pool_name'])
              raise NotFoundError if p.nil?

              instance_id = body[:instance_id]
              raise BadRequestError, "'instance_id' is required." if instance_id.nil?

              ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)
              instance = ec2_rsrc.instance(instance_id)

              raise BadRequestError, "Couldn't find instance '#{instance_id}' in region " \
                                     "#{Settings.aws.region}." unless instance.exists?



              set = p.sets.find do |vs|
                vs.availability_zone == instance.placement.availability_zone
              end

              if set.nil?
                status 409 # conflict
                { conflict: "No sets currently available in #{Settings.aws.region}." }
              else
                set_state = set.state
                raise BadRequestError, "set must be 'detached', currently '#{set.state.status}'." \
                                       unless set_state[:status] == :detached

                unmountable_volumes =
                  set.volumes.select do |v|
                    instance.block_device_mappings.any? do |bdm|
                      bdm.device_name == v.spec.mount_point
                    end
                  end.map { |v| v.spec.mount_point }

                raise BadRequestError, "instance #{instance.id} can't be attached to " \
                                       "because these mount points are blocked: " \
                                       "#{unmountable_volumes.join(', ')}" unless unmountable_volumes.empty?

                ebs_volume = nil # allowing fall-through for exceptions
                begin
                  logger.info "pool #{p.name}: attempting to attach set ##{set.ordinal} to #{instance.id}"
                  set.volumes.each do |volume|
                    logger.info "pool #{p.name}, set #{set.ordinal}: attaching #{volume.name} (#{volume.ebs_id})"
                    ebs_volume = ec2_rsrc.volume(volume.ebs_id)
                    ebs_volume.attach_to_instance(
                      device: volume.spec.mount_point,
                      instance_id: instance.id
                    )
                  end

                  logger.info "pool #{p.name}: successfully attached set ##{set.ordinal} to #{instance.id}"
                rescue StandardError => err
                  ebs_volume_id = ebs_volume.nil? ? 'nil' : ebs_volume.id
                  logger.error "pool #{p.name}, set #{set.ordinal}: Failed to attach " \
                               "'#{ebs_volume_id}': #{err}"
                  raise InternalServerError, "Failed to attach set ##{set.ordinal} to #{instance.id}; " \
                                             "volume set is in an inconsistent state and should be " \
                                             "detached once diagnosed. Failure from #{ebs_volume_id}: #{err}"
                end

                {
                  set_ordinal: set.ordinal,
                  ebs_ids: set.volumes.map do |v|
                    [ v.name, { mount_point: v.mount_point, ebs_id: v.ebs_id } ]
                  end.to_h
                }
              end
            end
          end

          resources :sets do
            desc 'Lists all VolumeSets.'
            get do
              resource_type = 'ebs-volume-pool'

              authorize!(type: resource_type, name: params['pool_name'], action: :info) do
                p = Models::VolumeSetPool.first(name: params['pool_name'])
                raise NotFoundError if p.nil?

                p.sets.map(&:to_response_hash).map { |h| [ h[:ordinal], h.except(:ordinal) ] }.to_h
              end
            end

            desc 'Gets the count of volume sets (to go with the related PUT).'
            get '/count' do
              resource_type = 'ebs-volume-pool'

              authorize!(type: resource_type, name: params['pool_name'], action: :info) do
                p = Models::VolumeSetPool.first(name: params['pool_name'])
                raise NotFoundError if p.nil?

                { count: p.count }
              end
            end

            desc 'Specify a new count of volume pools. Must be higher than the current count.'
            put '/count' do
              resource_type = 'ebs-volume-pool'
              body = request_body_as_json

              new_count = body[:count]
              raise BadRequestError, "no count provided" if new_count.nil?

              authorize!(type: resource_type, name: params['pool_name'], action: :expand) do
                p = Models::VolumeSetPool.first(name: params['pool_name'])
                raise NotFoundError if p.nil?

                raise BadRequestError, "new count must not be less than the current count (#{p.count})." \
                  if new_count < p.count

                p.expand!(new_count: new_count).map(&:to_response_hash)
              end
            end

            params do
              requires :set_ordinal, type: Integer, desc: 'VolumeSet ordinal'
            end

            route_param :set_ordinal do
              desc 'Gets information about a single VolumeSet.'
              get do
                resource_type = 'ebs-volume-pool'

                authorize!(type: resource_type, name: params['pool_name'], action: :info) do
                  p = Models::VolumeSetPool.first(name: params['pool_name'])
                  raise NotFoundError if p.nil?

                  volume_set = Models::VolumeSet.first(pool_id: p.id, ordinal: params['set_ordinal'].to_i)
                  raise NotFoundError if volume_set.nil?

                  volume_set.to_response_hash
                end
              end

              desc 'Deletes a single VolumeSet.'
              delete do
                status 501
                { error: "not implemented" }
              end

              desc 'forces detachment of all volumes in the VolumeSet.'
              post '/detach' do
                # TODO: a "block_until_detached" argument is probably a good idea.
                resource_type = 'ebs-volume-pool'
                body = request_body_as_json

                force_detach = !!body[:force_detach]

                authorize!(type: resource_type, name: params['pool_name'], action: :'detach') do
                  p = Models::VolumeSetPool.first(name: params['pool_name'])
                  raise NotFoundError if p.nil?

                  volume_set = Models::VolumeSet.first(pool_id: p.id, ordinal: params['set_ordinal'].to_i)
                  raise NotFoundError if volume_set.nil?

                  ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

                  logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}': attempting to detach"
                  volume_set.volumes.each do |volume|
                    ebs_volume = ec2_rsrc.volume(volume.ebs_id)

                    begin
                      ebs_volume.attachments.each do |attachment|
                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: detaching from #{attachment.instance_id} " +
                                    (force_detach ? "(forced)" : "")

                        ebs_volume.detach_from_instance(
                          instance_id: attachment.instance_id,
                          device: attachment.device,
                          force: force_detach
                        )
                      end
                    rescue Aws::EC2::Errors::InvalidVolumeNotFound
                      logger.error "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                   "volume #{volume.name}: EBS volume #{volume.ebs_id} deleted."
                    end
                  end

                  Models::VolumeSet[volume_set.id].to_response_hash
                end
              end

              params do
                requires :volume_name, type: String, desc: 'VolumeSetVolume name'
              end

              route_param :volume_name do
                desc 'Gets information about a single Volume.'
                get do
                  resource_type = 'ebs-volume-pool'

                  authorize!(type: resource_type, name: params['pool_name'], action: :info) do
                    p = Models::VolumeSetPool.first(name: params['pool_name'])
                    raise NotFoundError if p.nil?

                    volume_set = Models::VolumeSet.first(pool_id: p.id, ordinal: params['set_ordinal'].to_i)
                    raise NotFoundError if volume_set.nil?

                    volume = volume_set.volumes.find { |v| v.name == params['volume_name'] }
                    raise NotFoundError if volume.nil?

                    volume.to_response_hash
                  end
                end

                desc 'Updates a single Volume (allows setting the EBS volume ID).'
                put do
                  resource_type = 'ebs-volume-pool'
                  body = request_body_as_json

                  authorize!(type: resource_type, name: params['pool_name'], action: :'replace-volume') do
                    p = Models::VolumeSetPool.first(name: params['pool_name'])
                    raise NotFoundError if p.nil?

                    volume_set = Models::VolumeSet.first(pool_id: p.id, ordinal: params['set_ordinal'].to_i)
                    raise NotFoundError if volume_set.nil?

                    volume = volume_set.volumes.find { |v| v.name == params['volume_name'] }
                    raise NotFoundError if volume.nil?

                    old_volume = volume.ebs_id
                    new_volume = body[:ebs_id]

                    ec2_rsrc = Aws::EC2::Resource.new(region: Settings.aws.region)

                    if !new_volume.nil? && (new_volume != old_volume)
                      logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                  "volume #{volume.name}: volume PUT #{old_volume} to #{new_volume}"

                      dupe_vsv = Models::VolumeSetVolume.first(ebs_id: new_volume)
                      raise BadRequestError, "new volume #{new_volume} already a member of pool " \
                                             "#{dupe_vsv.set.pool.name}, set #{dupe_vsv.set.ordinal}" \
                        unless dupe_vsv.nil?

                      begin
                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: handling new volume #{new_volume}"
                        ebs_new_volume = ec2_rsrc.volume(new_volume)
                        ebs_new_volume.state

                        raise BadRequestError, "new volume #{new_volume} in AZ " \
                                               "'#{ebs_new_volume.availability_zone}', but volume " \
                                               "set #{volume_set.ordinal} is in AZ '#{volume_set.availability_zone}'." \
                          unless ebs_new_volume.availability_zone == volume_set.availability_zone

                        volume.ebs_id = ebs_new_volume.id
                        volume.ensure_tags
                        volume.save

                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: new volume updated successfully"
                      rescue Aws::EC2::Errors::InvalidVolumeNotFound
                        raise BadRequestError, "Volume '#{new_volume}' is not a valid volume."
                      end

                      begin
                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: handling old volume #{old_volume}"
                        ebs_old_volume = ec2_rsrc.volume(old_volume)

                        if !!body[:retain_old_volume]
                          ebs_old_volume.create_tags(
                            tags: {
                              'disklord:retired' => ''
                            }.map { |k, v| { key: k, value: v.to_s} }
                          )
                        else
                          ec2_rsrc.volume(old_volume).delete
                        end

                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: old volume cleaned up"
                      rescue Aws::EC2::Errors::InvalidVolumeNotFound
                        logger.info "'#{p.initial_value(:name)}', set '#{volume_set.ordinal}', " \
                                    "volume #{volume.name}: old volume apparently already deleted"
                      end
                    end

                    volume.to_response_hash
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
