require 'disklord/error'

module Disklord
  module API
    class APIError < Disklord::Error; end

    class BadRequestError < APIError; end
    class UnauthorizedError < APIError; end
    class ForbiddenError < APIError; end
    class NotFoundError < APIError; end

    class InternalServerError < APIError; end
  end
end
