require 'settingslogic'

module Disklord
  class Settings < Settingslogic
    source ENV['DISKLORD_SETTINGS_PATH'] || "/etc/disklord/settings.yaml"
  end
end
