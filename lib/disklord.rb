require 'active_support'
require 'active_support/core_ext'
require 'settingslogic'
require 'aws-sdk'
require 'grape'
require 'chronic_duration'

require 'ice_nine'

require 'json'
require 'pathname'
require 'lockfile'
require 'fileutils'
require 'parallel'

require 'wonk'
require 'disklord/version'
require 'disklord/settings'
require 'disklord/logging'

Wonk.aws_region = Disklord::Settings.aws.region
Wonk.logger = Disklord::Logging.logger

Dir["#{__dir__}/**/*.rb"] \
  .reject { |f| f.include?("/api/") } \
  .reject { |f| f.include?("/models/") } \
  .reject { |f| f.include?("models.rb") } \
  .each { |f| require_relative f }

module Disklord
end
