[ "#{__dir__}/lib", "#{__dir__}" ].each do |lib|
  $LOAD_PATH.unshift(lib) if File.directory?(lib) && !$LOAD_PATH.include?(lib)
end

require 'disklord'

logger Disklord::Logging.logger
worker_processes Integer(Disklord::Settings.unicorn.workers.count)
timeout Disklord::Settings.unicorn.timeout

iface = Disklord::Settings.unicorn.listen.interface
port = Disklord::Settings.unicorn.listen.port

listen "#{iface}:#{port}"

before_fork do |server, worker|
  Signal.trap 'TERM' do
    Disklord::Logging.logger.info \
      'Unicorn master intercepting TERM and sending myself QUIT instead'
    Process.kill 'QUIT', Process.pid
  end
end

after_fork do |server, worker|
  ::DB = Sequel.connect(Disklord::Settings.sequel.database_url) if defined?(Sequel)

  require 'disklord/models'

  Signal.trap 'TERM' do
    Disklord::Logging.logger.info \
      'Unicorn worker intercepting TERM and doing nothing. Wait for master to send QUIT'
  end
end