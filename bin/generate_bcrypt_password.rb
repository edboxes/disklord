#! /usr/bin/env ruby

require 'bcrypt'
require 'io/console'

$stderr.write "Enter password: "
$stdin.noecho do |io|
  pwd = BCrypt::Password.create(io.readline[0...-1])

  puts pwd
end
