[ "#{__dir__}/lib", "#{__dir__}" ].each do |lib|
  $LOAD_PATH.unshift(lib) if File.directory?(lib) && !$LOAD_PATH.include?(lib)
end

require 'disklord'
require 'disklord/api/v1'

run Disklord::API::V1
