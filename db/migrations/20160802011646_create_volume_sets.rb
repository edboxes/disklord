Sequel.migration do
  change do
    create_table(:volume_sets, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum
      foreign_key     :pool_id,
                      :volume_set_pools,            type: :Bignum,  null: false

      Integer         :ordinal,                     null: false
      String          :availability_zone,           null: false,  size: 16

      index           [ :pool_id, :ordinal ],       unique: true
    end

    create_table(:volume_set_volumes, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum
      foreign_key     :volume_set_id,
                      :volume_sets,                 type: :Bignum,  null: false
      foreign_key     :volume_spec_id,
                      :volume_specs,                type: :Bignum,  null: false

      String          :ebs_id,                      null: false,  size: 30

      index           :ebs_id,                              unique: true
      index           [ :volume_set_id, :volume_spec_id ],  unique: true
    end
  end
end
