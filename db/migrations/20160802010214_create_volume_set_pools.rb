Sequel.migration do
  change do
    create_table(:volume_set_pools, engine: 'InnoDB') do
      primary_key     :id,            type: :Bignum

      String          :name,          null: false,  size: 36
      String          :description,   null: false,  text: true

      index           [:name],        unique: true
    end

    create_table(:volume_set_pool_azs, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum
      foreign_key     :pool_id,
                      :volume_set_pools,            type: :Bignum,  null: false

      String          :availability_zone,           null: false,  size: 16

      index           [ :pool_id, :availability_zone ], unique: true
    end
  end
end
