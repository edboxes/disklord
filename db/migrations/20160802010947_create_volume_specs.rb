Sequel.migration do
  change do
    create_table(:volume_specs, engine: 'InnoDB') do
      primary_key     :id,                          type: :Bignum
      foreign_key     :pool_id,
                      :volume_set_pools,            type: :Bignum,  null: false

      String          :name,                        null: false,  size: 36
      String          :description,                 null: false,  text: true
      TrueClass       :encrypted,                   null: false,  default: false
      String          :mount_point,                 null: false,  size: 24
      String          :snapshot_id,                 null: true,   size: 30
      String          :kms_key_id,                  null: true,   size: 255
      Integer         :gigabytes,                   null: false
      String          :ebs_type,                    null: false,  size: 12
      Integer         :piops,                       null: true

      index           [ :pool_id, :name ],          unique: true
      index           [ :pool_id, :mount_point ],   unique: true
    end
  end
end
